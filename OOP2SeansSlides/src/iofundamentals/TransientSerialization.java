/**
 * 
 */
package iofundamentals;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Brian
 *
 */
public class TransientSerialization {
	public static void main(String[] args) {
		USPresident usPresident = new USPresident("Barrack Obama",
				"2009 to 2017", "56th and 57th terms");
		// Before serialisation
		System.out.println(usPresident);

		// Serialise the object
		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream("USPresident.data"))) {
			oos.writeObject(usPresident);
		} catch (Exception e) {
			System.err.println("Exception oocured...");
		}
		// De-Serialise the object
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
				"USPresident.data"))) {
			Object obj = ois.readObject();
			if (obj != null && obj instanceof USPresident) {
				USPresident president = (USPresident) obj;
				// After serialisation
				System.out.println(president);
			}
		} catch (Exception e) {
			System.err.println("Exception occured");
		}
	}
}
