/**
 * 
 */
package iofundamentals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Brian
 *
 */
public class Copy {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Character stream I/O
		String srcFile = "CopyRight.txt";
		String dstFile = "CopyRight2.txt";
		
		//try opening src and dest files
		//with FileReader and FileWriter
		//try with resources will auto close() all open resources
		//i.e. both FileReader and FileWriter objects
		try(BufferedReader inputFile = new BufferedReader(new FileReader(srcFile));
				BufferedWriter outputFile = new BufferedWriter(new FileWriter(dstFile))){
			int ch = 0;
			
			
			//comments from the Java API for read() and write() below
			while((ch = inputFile.read())!=-1){
				outputFile.write(ch);
			}
			//no need to call flush() explicitly for outputFile
		}
		catch (FileNotFoundException fnfe) {
			// TODO: handle exception
			System.err.println("Cannot open the file "+fnfe.getMessage());
		}
		catch (IOException e) {
			// TODO: handle exception
			System.err.println("Error when processing file; exiting...");
		}
	}

}
