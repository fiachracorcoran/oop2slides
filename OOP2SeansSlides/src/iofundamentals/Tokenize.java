/**
 * 
 */
package iofundamentals;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Brian
 *
 */
// read the input and convert into tokens of words
// convert the words to lower case ensure there are no duplicates and print the
// words
public class Tokenize {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// character stream I/O

		String fileName = "CopyRight.txt";
		// TreeSet<String> sorts the words in alphabetical order
		Set<String> words = new TreeSet<>();
		try (Scanner tokenizingScanner = new Scanner(new FileReader(fileName))) {
			// delimit based on non word characters(any chaacters a-zA-Z_0-9 e.g
			// whitespace)
			// setting a delimiter and not patten match so all words are
			// returned
			tokenizingScanner.useDelimiter("\\W");

			while (tokenizingScanner.hasNext()) {
				// next() returns an empty array when newLine is reached
				String word = tokenizingScanner.next();

				if (!word.isEmpty()) {
					// adds word to set
					words.add(word.toLowerCase());
				}
			}
			// TreeSet stores in alphabetical order
			// Set have no duplicates
			for (String word : words) {
				System.out.println(word + "\t");
			}
		} catch (FileNotFoundException e) {
			System.err
					.println("Cannot read the input file -- pass a valid file name");
		}
	}

}
