/**
 * 
 */
package iofundamentals;

import java.io.Serializable;

/**
 * @author Brian
 *
 */
// This class is used TransientSerialization and SerialVersionUIDTest
public class USPresident implements Serializable {

	private String name;
	private String peroid;

	// term is not serialised as in transient
	private transient String term;

	// due to new method getName() added change id from 1 to 2
	// private static final long serialVersionUID = 1L;
	private static final long serialVersionUID = 2L;

	@Override
	public String toString() {
		return "USPresident [name=" + name + ", peroid=" + peroid + ", term="
				+ term + "]";
	}

	public USPresident(String name, String peroid, String term) {
		super();
		this.name = name;
		this.peroid = peroid;
		this.term = term;
	}

	public String getName() {
		return name;
	}

}
