/**
 * 
 */
package iofundamentals;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Brian
 *
 */
public class DataStream {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(
				"temp.data"))) {
			for (int i = 0; i < 10; i++) {
				dos.writeByte(i);
				dos.writeShort(i);
				dos.writeInt(i);
				dos.writeLong(i);
				dos.writeFloat(i);
				dos.writeDouble(i);

			}
		} catch (IOException ioe) {
			System.err
					.println("an I/O error occured while processing the file");
			System.exit(-1);
		}
		try (DataInputStream dis = new DataInputStream(new FileInputStream(
				"temp.data"))) {
			for (int i = 0; i < 10; i++) {
				System.out.printf("%d %d %d %d %g %g %n", dis.readByte(),
						dis.readShort(), dis.readInt(), dis.readLong(),
						dis.readFloat(), dis.readDouble());
			}
		} catch (FileNotFoundException fnfe) {
			System.err.println("cannot read a file with the given file name");
		} catch (IOException ioe) {
			System.err.println("an io error occurred while processing");
		}
	}
}
