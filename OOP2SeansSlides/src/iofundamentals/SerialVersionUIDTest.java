/**
 * 
 */
package iofundamentals;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * @author Brian
 *
 */
public class SerialVersionUIDTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// deserialise the object
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
				"USPresident.data"))) {
			Object obj = ois.readObject();
			if (obj != null && obj instanceof USPresident) {
				USPresident president = (USPresident) obj;
				// After serialisation
				System.out.println(president);
			} else {
				System.out.println("error: " + obj);
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}

}
