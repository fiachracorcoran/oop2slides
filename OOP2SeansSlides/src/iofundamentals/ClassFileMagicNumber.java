/**
 * 
 */
package iofundamentals;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Brian
 *
 */
// Check if the passed file is a valid .class file or not.
// According to the JVM spec, the first four bytes of the .class file
// should be "0xCAFEBABE". This is known as the "magic number"
public class ClassFileMagicNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Byte Stram I/O

		// point this to your bin folder to get to the class file
		String fileName = "F:\\OOP2\\StuffForTest\\bin\\com\\iofundamentals\\Copy.class";
		// Create byte array with values for four bytes
		// you need to downcast to byte since
		// the hex values e.g. 0xCA
		byte[] magicNumber = { (byte) 0xCA, (byte) 0xFE, (byte) 0xBA,
				(byte) 0xBE };

		try (BufferedInputStream bis = new BufferedInputStream(
				new FileInputStream(fileName))) {
			byte[] u4buffer = new byte[4];

			if (bis.read(u4buffer) != -1) {
				if (Arrays.equals(magicNumber, u4buffer)) {
					System.out
							.printf("The magic number for %s matches a .class file format%n",
									fileName);
				} else {
					System.out
							.printf("The magic number for %s does not match a .class file format%n",
									fileName);

				}
			}
		} catch (FileNotFoundException fnfe) {
			// TODO: handle exception
			System.err.println("File not found");
		} catch (IOException e) {
			// TODO: handle exception
			System.err.println("An I/O error occured during processing");
		}

	}
}
