package regex;
import java.util.regex.Matcher; 
import java.util.regex.Pattern; 

public class GroupExample { 

	public static void main(String[] args) { 

		String emailToParse= "joe.bloggs@example.com";
		// Parse out the constituent e1ements... 

		Pattern regexPattern = Pattern.compile("(\\S+)\\.(\\S+)@(\\S+)"); 
		Matcher matcher = regexPattern.matcher(emailToParse); 
		while(matcher.find()) { 
			System.out.println("First Name: "+matcher.group(1)); // First Name: joe 
			System.out.println("Surnarne: "+matcher.group(2)); // Surname: bloggs 
			System.out.println("Domain: "+matcher.group(3)); // Domain: example.com 

			System.out.println("Everything: " +matcher.group(0)); 

			System.out.println("Everything: " +matcher.group()); 
		}
	}
}