package regex;

import java.util.regex.Matcher; 
import java.util.regex.Pattern; 

public class Regex2 { 

	public static void main(String[] args) { 

		String str = "Danny Doo, Hugo�city 56010, Ph: 9876543210. Maggi Myer, + Big bank city 56000, ph: 9876501234"; 

		Pattern pattern = Pattern.compile("\\d{5}"); // 5 digits
		Matcher matcher = pattern.matcher(str);

		while(matcher.find()) { 
			System.out.println(matcher.group()); 
		} 

	}
}