package regex;
import java.util.regex.Pattern;

//This program demonstrates how we can validate an IP address 

public class IPAddress { 

	void validatelP(String ipStr) { 
		// Sample IP address �255.245.188.123� 
		// \\b = word boundary 
		// e.g. �\\bdog\\b� is ok for a string �where is the clog� 
		// but not ok for �where is my doggie� // The first part is: 
		// 25[0�51 = 250..255 // 2[0�4]\\d = 200..249 
		// [01]?\\d\\d? = 0..199 // (\\.) = followed by a dot 
		// (ip_regex followed by a .){3} means �ip_regex.ip_regex.ip_regex.� 
		// The second part is the first part without a trailing �.� 
		String regex = "\\b((25[0�5]|2[0�4]\\d|[01]?\\d\\d?)(\\.)){3}" + 
				"(25[0�5]|2[0�4]\\d|[01]?\\d\\d?)\\b"; 
		System.out.println(ipStr + " is valid? " + Pattern.matches(regex, ipStr)); 
	}

	public static void main(String[] args) { 

		String ipStrl = "255.245.188.123"; 
		String ipStr2 = "255.245.188.273"; 
		IPAddress validator = new IPAddress(); 
		validator.validatelP(ipStrl); // 255.245.188.123 is valid? true 
		validator.validatelP(ipStr2); // 255.245.188.273 is valid? false
	}
}
