package regex;
 // This program demonstrates how we can manipulate text 

import java.util.regex.Matcher; 
import java.util.regex.Pattern; 

public class Regex6 { 
	
	public static void main(String[] args) { 
		String str = "Danny Doo, Flat no 502, Big Apartment, Wide Road, "
			 + "Near Huge Milestone, Hugo�city 56010, Ph: 0876543210, "
				+ "Email: danny@myworld.com. Maggi Myer, Post bag no 52," + 
			 "Big bank post office, , Big bank city 56000, "
			 + "ph: 0876501234, Email: maggi07@myuniverse.com";

			// �\D� � matches non�digits 
		Pattern pattern = Pattern.compile("(\\D)(\\d{3})(\\d{7})(\\D)");
		Matcher matcher = pattern.matcher(str); 
		String newStr = matcher. replaceAll("$1($2)-$3$4");
				
		System.out.println(newStr); 
		/* Output Danny Doo, F1at no 502, Big Apartment, 
		 * Wide Road, Near Huge Milestone, Hugo�city 56010, 
		 * Ph: (087)�6543210, Email: dannymyworld.com. 
		 * Maggi Myer, Post bag no 52, Big bank post office, 
		 * Big bank city 56000, ph: (087)�6501234, 
		 * Email: maggi07myuniverse. com. */
	}
}