package regex;


import java.util.regex.Matcher; 
import java.util.regex.Pattern; 

public class BackReference { 

	public static void main(String[] args){ 
		// Note: the two email, addresses must be the same for back 
		// references to work 
		String str = "jb@abc.com jb@abc.com"; 
		Pattern pattern = Pattern.compile("([a�z]+@[a�z]+\\.[a�z]{3})\\s\\1");
		Matcher matcher = pattern.matcher(str); 

		while(matcher.find()) { // do find() before group() else exception 
			System.out.println(matcher.group()); 
		} 
	}
}
