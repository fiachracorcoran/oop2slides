package regex;

import java.util.regex.Matcher; 
import java.util.regex.Pattern; 

public class RegexMulti { 

	public static void main(String[] args) { 

		String str = "Danny Doo, Hugo-city 56010, Ph: 9876543210. Maggi Myer, Big bank city 56000, ph: 9876501234"; 
		
		Pattern pattern0 = Pattern.compile("\\d{5}"); // 5 digits
		Matcher matcher0 = pattern0.matcher(str);

		while(matcher0.find()) { 
			System.out.println(matcher0.group()); 
		} 

		Pattern pattern = Pattern.compile("\\D\\d{5}\\D"); // 5 digit num preceeded by one non didgit char
		Matcher matcher = pattern.matcher(str);

		while(matcher.find()) { 
			System.out.println(matcher.group()); 
		} 
		
		Pattern pattern1 = Pattern.compile("\\b\\d{5}\\b"); // word boundary \b
		Matcher matcher1 = pattern1.matcher(str);
		
		while(matcher1.find()) { 
			System.out.println(matcher1.group()); 
		} 
		
		Pattern pattern2 = Pattern.compile("\\b[city]"); // "c" twice
		Matcher matcher2 = pattern2.matcher(str);
		
		while(matcher1.find()) { 
			System.out.println("Found: " + matcher2.group()); 
		} 

	}
}