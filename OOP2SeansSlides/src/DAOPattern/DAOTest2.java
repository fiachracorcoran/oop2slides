package DAOPattern;

//Adding in a new XML persistence mechanism. No Change at the business layer

import java.io.Serializable;

class Circle1{
	private int xPos, yPos;
	private int radius;
	public Circle1(int x, int y, int r){
		xPos = x;
		yPos = y;
		radius = r;
	}
	@Override
	public String toString() {
		return "Center = (" + xPos + "," + yPos+ ") and radius = " + radius;
	}
	public CircleTransfer1 getCircleTransferObject() {
		CircleTransfer1 circleTransfer = new CircleTransfer1();
		circleTransfer.setRadius(radius);
		circleTransfer.setxPos(xPos);
		circleTransfer.setyPos(yPos);
		return circleTransfer;
	}
}

class CircleTransfer1 implements Serializable {
	private int xPos, yPos, radius;
	public int getxPos() { return xPos;}
	public void setxPos(int xPos) {this.xPos = xPos;}
	public int getyPos() {return yPos;}
	public void setyPos(int yPos) {this.yPos = yPos;}
	public int getRadius() {return radius;}
	public void setRadius(int radius) {this.radius = radius;}
}

interface CircleDAO1 {
	public void insertCircle(CircleTransfer1 circleTransfer);
	public CircleTransfer1 findCircle(int id);
	public void deleteCircle(int id);
}

class RDBMSDAO1 implements CircleDAO1 {
	@Override
	public void insertCircle(CircleTransfer1 circle){
		System.out.println("insertCircle implementation");
	
	}
	@Override 
	public CircleTransfer1 findCircle(int id) {return null;}
	@Override
	public void deleteCircle(int id){}
	
}


class XMLDAO implements CircleDAO1 { // XML implementation
	@Override
	public void insertCircle(CircleTransfer1 circle) {}
	@Override
	public CircleTransfer1 findCircle(int id) { return null;}
	@Override
	public void deleteCircle(int id) {}
}


class DAOFactory1{
	public static CircleDAO1 getCircleDAO(String sourcetype){
		switch(sourcetype){
			case "RDBMS":
				return new RDBMSDAO1();
			case "XML":
				return new XMLDAO();
		}
		return null;
	}
}
public class DAOTest2 {

	public static void main(String[] args) {
		Circle1 circle = new Circle1(10,10,20);
		System.out.println(circle);
		CircleTransfer1 circleTransfer = circle.getCircleTransferObject();
		
		CircleDAO1 circleDAO_RDBMS = DAOFactory1.getCircleDAO("RDBMS");
		circleDAO_RDBMS.insertCircle(circleTransfer);
		CircleDAO1 circleDAO_XML = DAOFactory1.getCircleDAO("XML");
		circleDAO_XML.insertCircle(circleTransfer);

	}

}
