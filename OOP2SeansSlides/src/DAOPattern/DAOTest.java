package DAOPattern;

import java.io.Serializable;

class Circle{
	private int xPos, yPos;
	private int radius;
	public Circle(int x, int y, int r){
		xPos = x;
		yPos = y;
		radius = r;
	}
	@Override
	public String toString() {
		return "Center = (" + xPos + "," + yPos+ ") and radius = " + radius;
	}
	public CircleTransfer getCircleTransferObject() {
		CircleTransfer circleTransfer = new CircleTransfer();
		circleTransfer.setRadius(radius);
		circleTransfer.setxPos(xPos);
		circleTransfer.setyPos(yPos);
		return circleTransfer;
	}
}

class CircleTransfer implements Serializable {
	private int xPos, yPos, radius;
	public int getxPos() { return xPos;}
	public void setxPos(int xPos) {this.xPos = xPos;}
	public int getyPos() {return yPos;}
	public void setyPos(int yPos) {this.yPos = yPos;}
	public int getRadius() {return radius;}
	public void setRadius(int radius) {this.radius = radius;}
}

interface CircleDAO {
	public void insertCircle(CircleTransfer circle);
	public CircleTransfer findCircle(int id);
	public void deleteCircle(int id);
}

class RDBMSDAO implements CircleDAO {
	@Override
	public void insertCircle(CircleTransfer circle){
		System.out.println("insertCircle implementation");
	
	}
	@Override 
	public CircleTransfer findCircle(int id) {return null;}
	@Override
	public void deleteCircle(int id){}
	
}
class DAOFactory{
	public static CircleDAO getCircleDAO(String sourcetype){
		switch(sourcetype){
			case "RDBMS":
				return new RDBMSDAO();
		}
		return null;
	}
}
public class DAOTest {

	public static void main(String[] args) {
		Circle circle = new Circle(10,10,20);
		System.out.println(circle);
		CircleTransfer circleTransfer = circle.getCircleTransferObject();
		
		CircleDAO circleDAO_RDBMS = DAOFactory.getCircleDAO("RDBMS");
		circleDAO_RDBMS.insertCircle(circleTransfer);

	}

}
