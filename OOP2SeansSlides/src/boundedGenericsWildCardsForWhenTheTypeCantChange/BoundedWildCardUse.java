package boundedGenericsWildCardsForWhenTheTypeCantChange;

import java.util.ArrayList;
import java.util.List;

public class BoundedWildCardUse {

	//this method will accept anything that is either a Number or a subclass of Number, e.g. Double, Integer (any wrapper class)
	public static Double sum(List<? extends Number> numList){
		Double result = 0.0;
		for(Number num:numList){
			result += num.doubleValue();
		}
		
		return result;
	}
	
	public static void main(String args[]){
		//these guys aint declared as wildcard generics, so we can modify them all we want.
		List<Integer> intList = new ArrayList<>();
		List<Double> doubleList = new ArrayList<>();
		
		for(int i = 0; i<5; i++){
			intList.add(i);
			doubleList.add((double)i * i);
			
		}
		
		System.out.println("sum of intlist " + sum(intList));
		System.out.println("double list " + doubleList);
		System.out.println("sum of double list " + sum(doubleList) );
		
	}
}
