package enums;

public class EnumTest2 {
	PrinterType2 printerType;

	public EnumTest2(PrinterType2 printerType) {
		this.printerType = printerType;
	}
	
	public void feature(){
		switch(printerType){
			case DOTMATRIX:
				System.out.println("Dot matrix printers are economical and almost obsolete");
				break;
			case INKJET:
				System.out.println("Inkjet printers provide decent quality prints");
				break;
			case LASER:
				System.out.println("Laser printers provide best quality prints");
				break;
		}
		System.out.println("Print page capacity per minute: "+printerType.getPagePrintCapacity());
	}
	public static void main(String[] args){
		EnumTest2 enumTest1= new EnumTest2(PrinterType2.LASER);
		enumTest1.feature();
		EnumTest2 enumTest2= new EnumTest2(PrinterType2.INKJET);
		enumTest2.feature();
	}

}
