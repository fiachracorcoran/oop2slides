package enums;

public enum PrinterType2 {
	DOTMATRIX(5), INKJET(10), LASER(50);
	
	private int pagePrintCapacity;

	private PrinterType2(int pagePrintCapacity) {
		this.pagePrintCapacity = pagePrintCapacity;
	}

	public int getPagePrintCapacity() {
		return pagePrintCapacity;
	}
	
}
