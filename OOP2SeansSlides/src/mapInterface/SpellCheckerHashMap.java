package mapInterface;

import java.util.HashMap;
import java.util.Map;

public class SpellCheckerHashMap {
	public static void main(String a[]){
		Map<String, String> misSpeltWords = new HashMap<String, String>();
		misSpeltWords.put("calendur", "calendar");
		
		String sentence = "buy a calendur for 2015.";
		System.out.println("Sentence given: " + sentence);
		
		for (String word : sentence.split("\\W+")) {
			if(misSpeltWords.containsKey(word)){
				System.out.println("The correct spelling for " + word + " is " + misSpeltWords.get(word));
			}
			
		}
	}

}
