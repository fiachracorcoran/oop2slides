package mapInterface;

import java.util.NavigableMap;
import java.util.TreeMap;

public class sortedTreeMapNavigableMap {

	public static void main(String a[]){
		//treemap implements navigablemap
		//its a key - value pair system, so between the <> there has to be two types (dont matter if the same)
		//else you get a compiler error.
		NavigableMap<Integer, String> examScores = new TreeMap<>();
		
		examScores.put(10, "Fred");	//head of the map cos of 10
		examScores.put(30, "Udo");
		examScores.put(20, "Corey");
		examScores.put(40, "Kieran");	//tail of the map cos of 40
		
		//remember, treemaps are sorted automatically, so it'll print in order, even though I put Carla in before bianca
		System.out.println("just print the map " + examScores);
		//note, descendingMap NOT keySet, it'll print based on scores descending, so highest grade first.
		System.out.println("print descending order " + examScores.descendingMap()); 
		System.out.println("people that got 40 or more " + examScores.tailMap(40));
		System.out.println("people that got less than 40 " + examScores.headMap(40));
		System.out.println("lowest mark " + examScores.firstEntry());
		
		
	}
}
