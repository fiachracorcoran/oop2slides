package nCopiesAndCopyMethodFromCollectionsInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HowToCopyACollection {

	
	public static void main(String args[]){
		//ok, we want to copy from src to destination.
		//we want src to be immutable, but we want dest to be mutable.
		//so we use nCopies to populate src.
		//nCopies returns an immutable list.
		//how it works : the below code is gonna fill src with 5 integers each of a value of 10
		//we specify <? extends Number> to specify the type, cos nCopies is a generic method
		//if you said <? extends Double> you'd get compiler error, cos you passing a new Integer
		//if your getting compiler error, then either your left or right hand side is wrong.
		List<? extends Number> src = Collections.nCopies(5, new Integer(10));
		
		//ok our dest object is automatically a list with no objects in it.
		//if we tried to copy to this, we'd get an index out of bounds exception
		//to avoid this, we fill it with null objects that match the size of the src list, see the for loop below doing this
		List<Object> dest = new ArrayList<>();
		
		//we know src has 5 Integers, so we populate dest with 5 also
		
		for(int i = 0; i< 5; i++){
			dest.add(new Object());
			
		}
		System.out.println(dest.size());
		
		//N.B -- THE ORDER OF PARAM'S
		//the Collections copy method signature is:
		//public static <T> void copy (List<? super T> dest, List<? extends T> src)
		//so T in this example, is inferred as Number, I don't know how that gets decided
		//but basically, Integer is a sub class of Number, and Object is a super class of Number, so maybe compiler
		//has a way of workiing this out
		//anyway L NB THE ORDER OF THE PARAMS, dest is first, src is second.
		Collections.copy(dest, src);
		
		System.out.println("dest == " + dest);
	}
}
