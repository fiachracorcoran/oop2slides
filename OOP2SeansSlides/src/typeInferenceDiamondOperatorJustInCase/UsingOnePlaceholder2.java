package typeInferenceDiamondOperatorJustInCase;

//=====
//GO DOWN TO LINE 28 FOR TYPE INFERENCE, IN CASE IT IS EXPLICITLY ASKED.
//=====
class PairOfT<T>{
	T object1;
	T object2;
	
	public PairOfT(T one, T two) {
		this.object1 = one;
		this.object2 = two;
	}
	
	public T getFirstObj(){
		return this.object1;
	}
	
	public T getSecondObj(){
		return this.object2;
	}
	
}

public class UsingOnePlaceholder2 {

	public static void main(String gg []){
		//note the empty angle brackets (these guys: < > are type inference, 
		//thats all it is, you specify the type on the left and leave the right side blank
		//this also called the diamond operator.
		PairOfT<String> names = new PairOfT<>("Emily", "Bobby");
		
		//an example of NOT type inference, cos we specify the type on both sides.
		PairOfT<String> names2 = new PairOfT<String>("Emily", "Bobby");
		
	}
}
