package association;


class Counter {
	private int theValue;
	public Counter(int aValue){
		theValue = aValue;
	}
}

class Account {
	private Counter theNumber;
	private int theBalance;
	public Account (int aNumber, int aBalance){
		// composition - object is created *in* Account
		theNumber = new Counter (aNumber); // Account is a composite object containing a Counter object
		theBalance = aBalance;
	}
}
public class Composition {
	public static void main(String args[]){
		//When 'a1' goes out of scope, the objecy it refers to
		//is garbage collected, along with the counter object
		//when is inside it
		Account a1 = new Account(1, 1000);
	}

}
