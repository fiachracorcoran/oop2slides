package association;

class Counter1 {
	private int theValue;
	public Counter1(int aValue){
		theValue = aValue;
	}
}
class Account1 {
	private Counter1 theNumber; // will store the reference of the object passed in
	private int theBalance;
	public Account1 (Counter1 aCounter, int aBalance){

		theNumber =  aCounter; // Aggregation - object reference *passed in*
		theBalance = aBalance;
	}
}
public class Aggregation {

	public static void main(String[] args) {
		//Counter object *created here* and passed in to account
		Counter1 c1 = new Counter1(1);
		someMethod(new Account1(c1, 1000));
		
		//At this point, one cannot access the Account reference as it has
		//gone out of scope (and not been saved); however we can still
		//use the counter reference 'c1'..
	}
	public static void someMethod (Account1 acc){
		/// some logic
	}

}
