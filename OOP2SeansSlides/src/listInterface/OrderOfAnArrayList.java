package listInterface;

import java.util.ArrayList;
import java.util.List;
public class OrderOfAnArrayList {

	public static void main(String[] args) {
		List<Number> al = new ArrayList<Number>();
		List<Number> al2 = new ArrayList<>();
		
		List<Integer> arrayListOfInteger = new ArrayList<Integer>();
		arrayListOfInteger.add(4);
		arrayListOfInteger.add(4);
		arrayListOfInteger.add(3);
		arrayListOfInteger.add(0, 5);
		arrayListOfInteger.add(2, 6);
		System.out.println(arrayListOfInteger);
		
		
		List<Number> arrayListOfNumber = new ArrayList<Number>(arrayListOfInteger);
		System.out.println(arrayListOfNumber);
	}

}
