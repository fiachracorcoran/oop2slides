package listInterface;

import java.util.ArrayList;
import java.util.Iterator;

public class TestIterator {
	public static void main(String []arfs){
		ArrayList<Integer> nums = new ArrayList<>();
		for(int i = 0; i< 10; i++){
			nums.add(i);
		}
		System.out.println("orig " + nums);
		
		//how to get the iterator
		Iterator<Integer> numsInterator = nums.iterator();
		while(numsInterator.hasNext()){
			numsInterator.next();
			numsInterator.remove();
			System.out.println(nums);
		}
		
		System.out.println("list when all removed " + nums);
	
	}
}
