package overriding;

import java.io.EOFException;
import java.io.FileNotFoundException;

public class Shape {
	private void method1(){ System.out.println("Shape::method1()");}
	public void method2(){ System.out.println("Shape::method2()");}
	public void method3(){ System.out.println("Shape::method3()");}
	public void method4() throws ClassNotFoundException, EOFException{ System.out.println("Shape::method4()");}
	public void method5() throws ClassNotFoundException, EOFException{ System.out.println("Shape::method5()");}
}

class Circle extends Shape{
	public void method1() { System.out.println("Circle::method1()");} // not actually overriding (hence no @Override)
	
	@Override
	public void method2() { System.out.println("Circle::method2()");} // ok overriding
	
	// method 3() error - attempting to assign weaker access privelege
//	protected void method3() { System.out.println("Circle::method3()");}
	
	//method 4() error - overridden method does not throw FileNotFoundException
	//i.e do not throw new or broader checked exceptions in the overriding method
//	public void method4() throws ClassNotFoundException, FileNotFoundException{ System.out.println("Circle::method4()");}
	
	//narrower exception spec, - ok
	@Override
	public void method4() throws ClassNotFoundException{ System.out.println("Circle::method4()");}
	
	// unchecked exception - ok
	@Override
	public void method5() throws ArithmeticException, IllegalArgumentException{ System.out.println("Circle::method5()");}
	
	
	public static void main(String []s) throws ClassNotFoundException{
		new Circle().method1();
		new Circle().method2();
		new Circle().method3();
		new Circle().method4(); // must catch ClassNotFoundException (checked exception) or main must throw it
		new Circle().method5();
	}
}
/*
Output:
	Circle::method1()
	Circle::method2()
	Shape::method3()
	Circle::method4()
	Circle::method5()
*/
 
