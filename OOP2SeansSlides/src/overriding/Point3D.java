package overriding;

class Point2D {
	private int xPos, yPos;
	public Point2D(int x, int y) {
		xPos = x;
		yPos = y;
	}
	@Override
	public String toString() {
		return "x = " + xPos + ", y = " + yPos;
 	}
}
public class Point3D extends Point2D{
	private int zPos;
	
	// provide a public constructors that takes three arguments (x, y, and z values
	public Point3D(int x, int y, int z){
		//call the superclass constructor with two arguments
		//i.e., call the Point2D(int, int) from Point2D(int, int , int) constructor)
		super(10,20); //note that super is the first statement in the method
		zPos = z;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", z = " + zPos;
	}
	
	// to test if we extended correctly, call the toString method of a Point3D object
	public static void main(String[] args) {
		System.out.println(new Point3D(10, 20, 30));
	}

}
