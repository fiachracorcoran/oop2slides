package overriding;

public class PointV2 {
	private int xPos, yPos;
	
	public PointV2(int x, int y){
		xPos = x;
		yPos = y;
	}
	
	//override the equals method in Object to perform
	// "deep" comparison of two Point objects
	public boolean equals(PointV2 other){
		System.out.println("Point::equals() - ");
		if(other == null)
			return false;
		// two points are equal only if their x and y positions are equal
		if((xPos == other.xPos) && (yPos == other.yPos))
			return true;
		else
			return false;
	}
	public static void main(String[] args) {
		Object p1 = new Point(10,20);
		Object p2 = new Point(50,100);
		Object p3 = new Point(10,20);
		System.out.println("p1 equals p2 is " + p1.equals(p2));
		System.out.println("p1 equals p3 is " + p1.equals(p3));
	}

}