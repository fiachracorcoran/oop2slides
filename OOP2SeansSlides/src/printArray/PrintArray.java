package printArray;

import java.util.Arrays;

public class PrintArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int array[] = {1, 2, 3, 4, 5};
		
		System.out.println(array);
		//USE THE ARRAYS.TOSTRING TO PRINT ARRAYS EFFIENTLY INSTEAD OF FOR LOOPS,
		System.out.println(Arrays.toString(array));
	}

}
