package binarySearch;

import java.util.Arrays;

public class BinarySearchTest {

	public static void main(String a []){
		String[] stringArray = {"barry", "carla", "ann"};
		
		System.out.println("The given stringArray is " + Arrays.toString(stringArray));
		
		Arrays.sort(stringArray);
		
		System.out.println("stringArray after sorting " + Arrays.toString(stringArray));
		
		int index = Arrays.binarySearch(stringArray, "barry");
		
		System.out.println("the index value is " + index);
	}
}
