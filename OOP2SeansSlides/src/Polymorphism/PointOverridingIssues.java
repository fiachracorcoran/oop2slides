package Polymorphism;

public class PointOverridingIssues {
	private int xPos, yPos;
	
	public PointOverridingIssues(int x, int y) {
		xPos = x;
		yPos = y;
	}
	
//	public String toString() { //ok
//		return "x = " + xPos + ", y = " + yPos;
//	}
	
	// error - attempting to weaken access priveleges
//	protected String toString() {
//		return "x = " + xPos + ", y = " + yPos;
//	}
	
	// error - Object is not compatible with String
//	public Object toString() {
//		return "x = " + xPos + ", y = " + yPos;
//	}
	
	//complies but output is "Point@e1cba87"
	public String ToString() {
		return "x = " + xPos + ", y = " + yPos;
	}
	public static void main(String[] args) {
		System.out.println(new PointOverridingIssues(10, 20));
	}

}
