package Polymorphism;

public class Point {
	private int xPos, yPos;
	
	public Point(int x, int y) {
		xPos = x;
		yPos = y;
	}
	
	public static void main(String[] args) {
		// Passing a Point object to println automatically
		//invokes the toString method
		System.out.println(new Point(10,20));
		//output Point@19821f
		
		//rewritten main
		Object obj = new Point(10,20);
		System.out.println(obj);
	}
	//Change toString output by overriding Object::toString()
	public String toString() {
		return "x = " + xPos + ", y = " + yPos;
	}
	
}
