package composition;
class Point{
	private int xPos;
	private int yPos;
	public Point(int x, int y){
		xPos = x;
		yPos = y;
	}
	@Override
	public String toString(){
		return "(" + xPos + "," + yPos + ")";
	}
}

public class Circle {
	private Point center; // Circle "Contains" a point object
	private int radius;
	public Circle(int x, int y, int r){
		center = new Point(x,y);
		radius = r;
	}
	public String toString() {
		return "Center = " + center + " and radius = " + radius;
	}
	public static void main(String[] args) {
		// Output is " Enter = (10, 10) and radius = 20"
		System.out.println(new Circle(10, 10, 20));

	}

}
