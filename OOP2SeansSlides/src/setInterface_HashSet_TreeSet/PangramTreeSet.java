package setInterface_HashSet_TreeSet;

import java.util.Set;
import java.util.TreeSet;

public class PangramTreeSet {
	
	public static void main(String [] args){
		String pangram = "the quick brown fox jumps over the lazy dog";
		
		//treesets are ordered, so we used them to order our characters alphabetically.
		Set<Character> letterAToletterZ = new TreeSet<>();
		
		for (char gram: pangram.toCharArray()) {
			letterAToletterZ.add(gram);	//cos we use TreeSet, we add no duplicates to the Set
			
		}
		System.out.println("the pangram is " + pangram);
		
		System.out.println("sorted pangram characters are \n " + letterAToletterZ);
		//the white space is normal, it's part of the ASCII alphabet, its 32
	}

}
