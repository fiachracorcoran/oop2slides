package listIteratorPalindrome;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ListIteratorTest {

	public static void main(String[] args) {
		String palindromeText = "navan";
		
		List<Character> palindromeArray = new LinkedList<>();
		
		for (Character character : palindromeText.toCharArray()) {
			palindromeArray.add(character);
		}
		
		System.out.println("input string is " + palindromeText);
		
		//N.B. listIterator()  NOT iterator();
		//note the I on Iterator is uppercase, in case you gettng compile error
		ListIterator<Character> forwardIter = palindromeArray.listIterator();
		ListIterator<Character> reverseIter = palindromeArray.listIterator(palindromeArray.size());
		
		boolean isPalindrome = true;
		
		while(reverseIter.hasPrevious() && forwardIter.hasNext()){
			if(forwardIter.next() != reverseIter.previous()){
				isPalindrome = false;
				break;
			}
		}
		if(isPalindrome){
			System.out.println("yeah its a palindrom");
		}else{
			System.out.println("no its not a palindrom");
		}
		
		
	}

}
