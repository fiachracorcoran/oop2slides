package utitlityClasses;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PlayList {

	public static void main(String a[]){
		List<String> playList = new LinkedList<>();
		playList.add("elvis");
		playList.add("joe cocker");
		playList.add("tiesto");
		
		System.out.println("orig: " + playList);
		
		System.out.println("reverSed playlist ");
		//nb
		Collections.reverse(playList);
		System.out.println(playList);
		
		System.out.println("sorted on names");
		Collections.sort(playList);
		System.out.println(playList);
		
		System.out.println("is elvis in the list? ");
		
		String elvis = "elvis";
		//NB NB NB NB NB NB NB NB NOTE THE ORDER OF THE PARAMS.
		//the list HAS TO BE SORTED FIRST BEFORE A BINARY SEARCH IS DONE.
		int index = Collections.binarySearch(playList, elvis);
		if(index >= 0){
			System.out.printf("yup he is %d \n", (index + 1));
		}else{
			System.out.printf("no, he not on the list \n");
		}
	}
}
