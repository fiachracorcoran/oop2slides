package TaggingInterface;

interface WishfulThinking{} //tagging interface

class SK implements WishfulThinking{
	@Override
	public String toString(){return "SK";}
	public String sportingIcon(){return "Roger Federer";}
}

// unrelated classes (to SK)
class LoadsOfMoney implements WishfulThinking{
	@Override
	public String toString(){return "LoadsOfMoney";}
	public String howTo(){return "Work Hard";}
}

class TheBossAtHome implements WishfulThinking{
	@Override
	public String toString(){return "TheBossAtHome";}
	public String instructions(){return "Wash Up...";}
}

class CherylCole implements WishfulThinking{
	@Override
	public String toString(){return "CherylCole";}
	public String advice(){return "Sing...";}
}
public class TaggingInterfaces2 {

	public static void main(String[] args) {
		someMethod(new LoadsOfMoney());
		someMethod(new TheBossAtHome());
		someMethod(new CherylCole());
		someMethod(new SK());

	}
	public static void someMethod(WishfulThinking wt){
		System.out.println(wt + " - ");
		if(wt instanceof LoadsOfMoney){
			System.out.println(((LoadsOfMoney)wt).howTo());
		}else if(wt instanceof TheBossAtHome){
			System.out.println(((TheBossAtHome)wt).instructions());
		}else if(wt instanceof CherylCole){
			System.out.println(((CherylCole)wt).advice());
		}else if(wt instanceof SK){
			System.out.println("My sporting icon is "+((SK)wt).sportingIcon());
		}
	}

}

//Output
// LoadsOfMoney..
// TheBossAtHome..
// CherlyCole..
// Sk...