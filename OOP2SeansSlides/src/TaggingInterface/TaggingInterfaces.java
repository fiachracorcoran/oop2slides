package TaggingInterface;

interface I0{} //tagging interface
class C1 implements I0{ // C1 and C2 are unrelated Classes
	public void a() {System.out.println("C1::a()");}
}
class C2 implements I0{
	public void b(){System.out.println("C2::b();");}
}



public class TaggingInterfaces {

	public static void main(String[] args) {
		someMethod(new C1());
		someMethod(new C2());

	}
	static void someMethod(I0 i0){
		if(i0 instanceof C1)
			((C1)i0).a();
		else
			((C2)i0).b();
	}
}

// Output
// C1::a()
// C2::b()
