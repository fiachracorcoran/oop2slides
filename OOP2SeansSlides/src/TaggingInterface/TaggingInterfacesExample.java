package TaggingInterface;

interface FallenOutWith{}// tagging interface
class SAF implements FallenOutWith{
	public boolean haveHugeEgo(){return true;}
}

// unrelated classes (to SAF)
class RoyKeane implements FallenOutWith{
	public String callItAsItIs(){return "They have there... ";}
}

class DavidBeckham implements FallenOutWith{
	public String whatPoshSaid(){return "If SAF... ";}
}

class JaapStam implements FallenOutWith{
	public boolean authorOfCriticalBook(){return true;}
}

class JohnMagnier implements FallenOutWith{
	public String rumour() {return "was it over breeding rights??";}
}
public class TaggingInterfacesExample {

	public static void main(String[] args) {
		someMethod(new RoyKeane());
		someMethod(new DavidBeckham());
		someMethod(new JaapStam());
		someMethod(new JohnMagnier());
		someMethod(new SAF());

	}
	public static void someMethod(FallenOutWith fow){
		if(fow instanceof DavidBeckham){
			System.out.println(((DavidBeckham)fow).whatPoshSaid());
		}else if(fow instanceof JohnMagnier){
			System.out.println(((JohnMagnier)fow).rumour());
		}else if(fow instanceof RoyKeane){
			System.out.println(((RoyKeane)fow).callItAsItIs());
		}else if(fow instanceof JaapStam){
			System.out.println("Did i write a critical book "+((JaapStam)fow).authorOfCriticalBook());
		}else if (fow instanceof SAF){
			System.out.println("Do I have a huge ego: "+ ((SAF)fow).haveHugeEgo());
	}
	}

}
