package observerPattern;

import java.util.Observable;
import java.util.Observer;

class Point{
	private int xPos;
	private int yPos;
	public Point(int x, int y){
		xPos = x;
		yPos = y;
	}
	@Override
	public String toString(){
		return "(" + xPos + "," + yPos + ")";
	}
}

class Circle extends Observable{
	private Point center;
	public void setCenter(Point center){
		this.center = center;
		setChanged();
		notifyObservers();
	}
	public void setRadius(int radius){
		this.radius = radius;
		setChanged();
		notifyObservers();
		
	}
	private int radius;
	public Circle(int x, int y, int r){
		center = new Point(x,y);
		radius = r;
	}
	
	@Override
	public String toString() {
		return "Center = " + center + " and radius "+ radius;
	}
}

class Canvas implements Observer {
	@Override
	public void update(Observable arg0, Object arg1){
		System.out.println("Canvas::update");
	}
	
}
class ShapeArchiver implements Observer{
	@Override
	public void update(Observable arg0, Object arg1){
		System.out.println("ShapeArchiever::update");
	}
}

public class Test {

	public static void main(String[] args) {
		Circle circle = new Circle(10, 10, 20);
		System.out.println(circle);
		circle.addObserver(new Canvas());
		circle.addObserver(new ShapeArchiver());
		circle.setRadius(50);
		System.out.println(circle);

	}

}
