package queueInterface;

import java.util.LinkedList;
import java.util.Queue;

public class QueueTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Queue is an interface which linkedlist implements.
		Queue<String> loginSequence = new LinkedList<>();
		
		loginSequence.add("Corey");
		loginSequence.add("Udo");
		loginSequence.add("Nigel");
		loginSequence.add("Terence");
		
		System.out.println("The loging sequence is " + loginSequence);
		
		while(!loginSequence.isEmpty()){
			System.out.println("removing " + loginSequence.remove());
		}

	}

}
