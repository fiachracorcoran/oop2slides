package appcanvas;

import graphicshape.Circle;
import graphicshape.Shape;

public class Triangle extends Shape{
	public void testAccess() {
		color=255; //ok
		
		Triangle t = new Triangle();
		t.color = 65; //ok
		
		Circle c = new Circle();
//		c.color=5; // no access
		
		Shape s = new Shape();
//		s.color=99; // no access
	}
}
