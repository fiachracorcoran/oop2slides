package singletonPattern;

import java.util.Arrays;

class A { // Traditional Method
	private static A singleton = new A();
	
	private A() {}
	
	public static A getInstance() {
		return singleton;
	}
}
// end of traditional Method



// Since Java 1.5 enums can be used
enum Elvis {
	INSTANCE;
	private final String[] favSongs = {"Hound Dog" , "HeartBreak Hotel"};
	public void printFav() {
		System.out.println(Arrays.toString(favSongs));
	}
}
public class TestSingleton {

	public static void main(String[] args) {
	//	A a1 = new A(); // Cannot new the class because the constructor is private
		A a = A.getInstance();
		
		//using an enum to generate a single instance
		Elvis elvis =Elvis.INSTANCE;
		elvis.printFav();
	}

}
