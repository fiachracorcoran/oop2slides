package BasicInterfaces;

interface Rollable {
	void roll(float degree);

}


//An abstract class does not need to define the methods...
abstract class CircularShape implements Rollable{}


public class Circle extends CircularShape{
	//public class Circle implements Rollable
	//Note: using the signature above "void roll(float degree)" gives a 
	//compiler error as the compiler detects the weaker access privileges
	// public -> package-private
	
	@Override
	public void roll(float degree){
		//some logic here...
	}
}
