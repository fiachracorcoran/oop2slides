package dequeInterface;

import java.util.ArrayDeque;
import java.util.Deque;

class SplQueue{
	private Deque<String> splQueue = new ArrayDeque<>();
	
	void addInQueue(String customer){
		splQueue.addLast(customer);
	}
	
	void removeFront(){
		splQueue.removeFirst();
	}
	
	void removeLast(){
		splQueue.removeLast();
	}
	
	void printQueue(){
		System.out.println("Queue contains " + splQueue);
	}
}

public class SplQueueTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SplQueue specialQ = new SplQueue();
		
		specialQ.addInQueue("Larry");
		specialQ.addInQueue("Barry");
		specialQ.addInQueue("Harry");
		specialQ.addInQueue("Garry");
		
		specialQ.printQueue();
		specialQ.removeFront();
		specialQ.removeLast();
		specialQ.printQueue();
	}

}
