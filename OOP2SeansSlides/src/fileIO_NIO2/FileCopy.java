/**
 * 
 */
package fileIO_NIO2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author Brian
 *
 */
public class FileCopy {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Path pathSource = Paths.get("CopyRight.txt");
		Path pathSource = Paths.get("C:\\Users\\Brian\\Desktop\\someDir");
		Path pathDest = Paths.get("C:\\Users\\Brian\\Desktop\\someDir2");

		try {
			// if copying a directory then only the directory file is copied
			// the contents is not

			// replace existing allows for overwriting
			// default throw FileAlreadyExistsException
			Files.copy(pathSource, pathDest,
					StandardCopyOption.REPLACE_EXISTING);
			System.out.println("Source file copied Successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
