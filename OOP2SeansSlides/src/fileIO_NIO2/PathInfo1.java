/**
 * 
 */
package fileIO_NIO2;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Brian
 *
 */
public class PathInfo1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// change to a dirctory of your choice
		Path testFilePath = Paths
				.get("C:\\Users\\Brian\\Desktop\\someDir\\someDir\\anotherDir\\test.txt");

		System.out.println("Printing file info");
		System.out.println("File.separator: " + File.separator);
		System.out.println("\t file name: " + testFilePath.getFileName());
		System.out.println("\t root of the path: " + testFilePath.getRoot());

		System.out.println("\t parent of the target: "
				+ testFilePath.getParent());

		System.out.println("Printing the elements of the path: ");

		for (Path element : testFilePath) {
			System.out.println("\t path element: " + element);
		}
	}

}
