/**
 * 
 */
package fileIO_NIO2;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Brian
 *
 */
// Walks the file tree of source and copies the folders and files to dest
class MyFileFindVisitorGlob extends SimpleFileVisitor<Path> {
	private PathMatcher matcher;

	/**
	 * 
	 */
	public MyFileFindVisitorGlob(String pattern) {
		try {
			matcher = FileSystems.getDefault().getPathMatcher(pattern);
		} catch (IllegalArgumentException iae) {
			System.err
					.println("Invalid pattern; did you forget to prefix \"glob:\"? (as in glob:*.xml");
			System.exit(-1);
		}
	}

	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {

		Path name = path.getFileName();
		System.out.println("File: " + name);
		if (matcher.matches(name)) {
			System.out.println("\tMatches");
		} else {
			System.out.println();
		}

		return FileVisitResult.CONTINUE;
	}

}

public class FileTreeWalkFindGlob {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Path startPath = Paths.get("C:\\Users\\Brian\\Desktop");
		String pattern = "glob:*.txt";

		try {
			Files.walkFileTree(startPath, new MyFileFindVisitorGlob(pattern));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
