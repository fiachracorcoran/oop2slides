/**
 * 
 */
package fileIO_NIO2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Brian
 *
 */
public class FileDelete {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Path pathSource = Paths
				.get("C:\\Users\\Brian\\Desktop\\someDir2\\CopyRight2.txt");

		try {
			Files.delete(pathSource);
			System.out.println("Source file deleted successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
