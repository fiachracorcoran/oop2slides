/**
 * 
 */
package fileIO_NIO2;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Brian
 *
 */
public class FilePermissions {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Path path = Paths.get("CopyRight.txt");
		System.out.printf("Readable: %b, Writable: %b, Executable: %b",
				Files.isReadable(path), Files.isWritable(path),
				Files.isExecutable(path));
	}
}
