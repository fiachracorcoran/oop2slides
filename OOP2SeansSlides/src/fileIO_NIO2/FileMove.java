/**
 * 
 */
package fileIO_NIO2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author Brian
 *
 */
public class FileMove {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Path pathSoucre = Paths.get("CopyRight2.txt");
		Path pathDest = Paths
				.get("C:\\Users\\Brian\\Desktop\\someDir2\\CopyRight2.txt");

		try {
			Files.move(pathSoucre, pathDest,
					StandardCopyOption.REPLACE_EXISTING);
			System.out.println("Source file moved successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
