/**
 * 
 */
package fileIO_NIO2;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Brian
 *
 */
public class PathInfo2 {
	public static void main(String[] args) {
		Path testFilePath = Paths.get("./Test");
		System.out.println("The file name is: " + testFilePath.getFileName());
		System.out.println("Its URI us: " + testFilePath.toUri());
		System.out.println("Its absolute path is: "
				+ testFilePath.toAbsolutePath());
		System.out.println("Is normailzed path is " + testFilePath.normalize());
	}
}
