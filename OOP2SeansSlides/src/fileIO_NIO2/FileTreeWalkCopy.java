/**
 * 
 */
package fileIO_NIO2;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Brian
 *
 */
// Walks the file tree of source and copies the folders and files to dest
class MyFileCopyVisitor extends SimpleFileVisitor<Path> {
	private Path source, dest;

	/**
	 * 
	 */
	public MyFileCopyVisitor(Path s, Path d) {
		source = s;
		dest = d;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path path,
			BasicFileAttributes attrs) {
		System.out.println("preVisitDirectory::path == " + path);
		Path newd = dest.resolve(source.relativize(path));
		try {
			Files.copy(path, newd, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {

		System.out.println("\tvisitFile::path == " + path);

		// See API for relativize and resolve

		System.out.println("\t\tsource.relativize(path) == "
				+ source.relativize(path));
		Path newd = dest.resolve(source.relativize(path));
		System.out.println("\t\tnewd == " + newd);
		try {
			Files.copy(path, newd, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return FileVisitResult.CONTINUE;
	}

}

public class FileTreeWalkCopy {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Path pathSource = Paths.get("C:\\Users\\Brian\\Desktop\\someDir");
		Path pathDest = Paths.get("C:\\Users\\Brian\\Desktop\\someDir2");

		try {
			Files.walkFileTree(pathSource, new MyFileCopyVisitor(pathSource,
					pathDest));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
