/**
 * 
 */
package fileIO_NIO2;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Brian
 *
 */
// Walks the file tree of source and copies the folders and files to dest
class MyFileFindVisitor extends SimpleFileVisitor<Path> {
	private String fileToFind;

	/**
	 * 
	 */
	public MyFileFindVisitor(String fileName) {
		fileToFind = fileName;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path path,
			BasicFileAttributes attrs) {
		System.out.println("preVisitDirectory::path.getName() == "
				+ path.getFileName());
		if (path.getFileName().toString().equals(fileToFind)) {
			System.out.println("Found directory!");
			return FileVisitResult.TERMINATE;
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {

		System.out.println("visitFile::path.getFileNAme() == "
				+ path.getFileName());

		if (path.getFileName().toString().equals(fileToFind)) {
			System.out.println("Found file!");
			return FileVisitResult.TERMINATE;
		}
		return FileVisitResult.CONTINUE;
	}

}

public class FileTreeWalkFind {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Path pathSource = Paths.get("C:\\Users\\Brian\\Desktop\\someDir");
		String fileToFind = "test.txt";

		try {
			Files.walkFileTree(pathSource, new MyFileFindVisitor(fileToFind));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
