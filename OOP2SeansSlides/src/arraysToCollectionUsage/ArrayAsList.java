package arraysToCollectionUsage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArrayAsList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Double[] weeklyTemp = { 31.2, 33.4, 95.6, 12.3};
		
		List<Double> temperaturesList = Arrays.asList(weeklyTemp);
		System.out.println("max temp " + Collections.max(temperaturesList));
		System.out.println("min temp " + Collections.min(temperaturesList));
	}

}
