package wildcards;

import java.util.ArrayList;
import java.util.List;

public class Wildcards {
	public static void main(String args[]){
		List<?> listOne = new ArrayList<Integer>();
		List<?> listTwo = new ArrayList<>();
		
//		list2.add( new Integer(7)); //COMPILER ERROR, YOU CAN'T MODIFY WILDCARD GENERIC.
	}
}
