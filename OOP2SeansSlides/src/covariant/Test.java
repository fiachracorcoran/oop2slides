package covariant;

abstract class Shape{
	public abstract Shape copy();
}
class Circle extends Shape{
	// Not using covariant return types (this was the way you had to override the method
	// prior to Java 5) i.e the return type was Shape in the superclass and therefore
	// had to be Shape in the subclass
	@Override
	public Shape copy() {
		return this;
	}
}
class Triangle extends Shape{
	// Using covariant return types; now since Java 5, we can override the method with
	// a subclass of the return type defined in the parent method
	@Override
	public Triangle copy() {
		return this;
	}
	
}
public class Test {
	Circle c1 = new Circle();
	// error on next line - incompatible types : required Circle; found Shape
//	Circle c2 = c1.copy();
	
	Circle c2 = (Circle)c1.copy(); // ok - cast required
	
	//Triangle::copy() uses covariant return types
	//i.e returns a subclass of the return type specified in the parent method
	Triangle t1 = new Triangle();
	Triangle t2 = t1.copy(); 	// no cast required here (neater)
}
