package ClassFundamentals;

class Overloaded {
	public static void aMethod (int val)	{ System.out.println("int");	}
	public static void aMethod (short val)	{ System.out.println("short");	}
	public static void aMethod (Object val)	{ System.out.println("object");	}
	public static void aMethod (String val)	{ System.out.println("String");	}
	
	public static void main(String[] args) {
		byte b = 9;
		aMethod(b);		// first call goes to short
		aMethod(9);		// second call goes to int
		Integer i = 9;
		aMethod(i);		// third call goes to Object
		aMethod("9");	//fourth call goes to String
	}

}
