package ClassFundamentals;

public class CircleWithProblemNotUsingThisKeyword {
	//this part is the same...
	
	//overloaded constructor
	public CircleWithProblemNotUsingThisKeyword(int xPos, int yPos, int radius) {
		xPos = xPos;
		yPos = yPos;
		radius = radius;
	}
	//this part is the same
}
//in some other method, the below outputs
//center = (0,0) and radius = 0
//System.out.println(new Circle(10,10,20).toString());


//Should be using this.xPos etc as it currently has not effect
//because it is assigning the value to the parameter variable instead
//of the classes variable