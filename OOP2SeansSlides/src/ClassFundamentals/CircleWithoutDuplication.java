package ClassFundamentals;

public class CircleWithoutDuplication {
	private int xPos, yPos, radius;
	
	// three overloaded constructors for Circle
	public CircleWithoutDuplication(int x, int y, int rad) {
		xPos = x;
		yPos = y;
		radius = rad;
	}
	public CircleWithoutDuplication(int x, int y) {
		this(x,y,10);
	}
	public CircleWithoutDuplication(){
		this(20,20,10);
	}
	public String toString() {
		return "center = (" + xPos + "," + yPos + ") and radius = " + radius;
	}
	public static void main(String[]s) {
		System.out.println(new CircleWithDuplication());
		System.out.println(new CircleWithDuplication(50,100));
		System.out.println(new CircleWithDuplication(25, 50, 5));
	}
}