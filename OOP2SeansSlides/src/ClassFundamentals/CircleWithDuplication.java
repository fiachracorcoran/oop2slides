package ClassFundamentals;

class CircleWithDuplication {
	private int xPos, yPos, radius;
	//default constructor initializing all three fields
	public CircleWithDuplication() {
		xPos = 20; //assume some default values for xPos and yPos
		yPos = 20;
		radius = 10; // default radius
	}
	//overloaded constructor
	public CircleWithDuplication(int x, int y, int rad) {
		xPos = x;
		yPos = y;
		radius = rad;
	}
	public CircleWithDuplication(int x, int y) {
		xPos = x;
		yPos = y;
		radius = 10;
	}
	public String toString() {
		return "center = (" + xPos + "," + yPos + ") and radius = " + radius;
	}
	public static void main(String[]s) {
		System.out.println(new CircleWithDuplication());
		System.out.println(new CircleWithDuplication(50,100));
		System.out.println(new CircleWithDuplication(25, 50, 5));
	}
}

// in some other method, the below outputs
//		center = (10,10) and radius = 20
//System.out.println(new Circle(10,10,20).toString());