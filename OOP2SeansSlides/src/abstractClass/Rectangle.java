package abstractClass;


// Shape is the base class for all shape objects; shape objects that are associated with
// a parent shape object is remembered in the parentShape field

abstract class Shape{
	abstract double area();
	private Shape parentShape;
	public void setParentShape(Shape shape) {parentShape = shape;}
	public Shape getparentShape() {return parentShape;}
}

//Rotable interface can be implemented by shapes such as Square, Rectangle, and Rhombus
// Cannot have a public interface and public class in same file.

interface Rotatable {
	void rotate(float degree);
}

//Rollable interface can be implemented by circular shapes such as Circle and Ellipse
interface Rollable {
	void roll(float degree);
}

//Rectangle is a concrete class and "is-a" Shape; It can be rotated and hence implements Rotatable
public class Rectangle extends Shape implements Rotatable {
	private int length, height;
	public Rectangle(int l, int h){length = l; height = h;}
	public double area() {return length * height; }
	@Override
	public void rotate(float degree) {}
}

// Circle is a concrete class that "is-a" subtype of Shape; you can roll it and hence implements Rollable
class Circle extends Shape implements Rollable {
	private int xPos, yPos, radius;
	public Circle(int x, int y, int r){
		xPos = x;
		yPos = y;
		radius = r;
	}
	public double area() {return Math.PI * radius * radius; }
	@Override
	public void roll(float degree){}
}
