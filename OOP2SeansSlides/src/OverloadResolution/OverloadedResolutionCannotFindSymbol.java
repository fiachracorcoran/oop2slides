package OverloadResolution;

public class OverloadedResolutionCannotFindSymbol {
	public static void aMethod (byte val) {System.out.println("byte"); }
	public static void aMethod (short val) {System.out.println("short"); }
	
	public static void main(String[] args) {
		aMethod(9); //results in "cannot find symbol" compiler error
//		compiler considers upcasts but not downcasts
//		as there is the risk of losing information.
	}
}
