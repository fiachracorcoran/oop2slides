package OverloadResolution;

public class OVerloading {
	public static void getDetails() throws IOException{
		
	}
	public static void getDetails(String s){ // ok
		
	}
	//different return type -> error - method already exists
	public static int getDetails(){
		
	}
	//different exception spec -> error - method already exists
	public static void getDetails() throws EOException{
		
	}
}
