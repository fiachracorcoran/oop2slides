package stringformatting;

import java.util.Calendar;
import java.util.GregorianCalendar;

class FormattedTable { 
	static void line() { 
		System.out.println("--------------------------------------------------------------------------------");
	}
	static void printHeader() { 
		System.out.printf("%-10s \t %s \t %s \t %s \t %s \n", "Player", "Matches", "Goals", "Goals per match", "Top player"); 

	}	

	static void printRow(String player, int matches, int goals, boolean topPlayer) { 
		System.out.printf("%-10s \t %5d \t\t %d \t\t %.1f \t\t %b\n", 
				player, matches, goals, ((float)goals/(float)matches), topPlayer); 
	}

	public static void main(String[] str) { 

		FormattedTable.line(); 
		FormattedTable.printHeader(); 
		FormattedTable.line(); 
		FormattedTable.printRow("Demando", 109, 122, false); 
		FormattedTable.printRow("Mushi", 80, 140, true); 
		FormattedTable.printRow("Peale", 150, 180, false); 
		FormattedTable.line(); 

		Calendar c = new GregorianCalendar(2000, 1, 13); 


		System.out.printf( "%s %5$td\t %s %5$tB\t %s %5$tY", 
						"The date is \n","\tDay:", 
						"\tMonth:", "\tYear:", c.getTime());
	}
}


