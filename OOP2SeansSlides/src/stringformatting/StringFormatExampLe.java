package stringformatting;

import java.util.Calendar; 
import java.util.GregorianCalendar; 

public class StringFormatExampLe{ 
	public static void main(String args[]) { 
		// Even though 2 is not wide enough, 40021 is output 
		// �Order with OrdId : 40021 and Amount: 3000 is missing� 
		System.out.printf("Order with OrdId : %2d and Amount: %10d is missing \n ", 40021, 3000); 
		String str = String.format("Hello %s", "Sean"); 
		System.out.println(str); 
		
		//common meta characters used in System.out.printf() method: 
		// %s � String, d � decimal, integer, %f � float 
		// %tD � date as mm/dd/yy, %td is day, %tm is month 
		// 96ty is 2 digit year, tY is four digit year 
		// %n is a newline 
		// Today�s date is 06/10/14 
		
		System.out.printf("Today�s date is tD %n", new GregorianCalendar()); 
		
		Calendar today = new GregorianCalendar(); 
		// Date in dd/mm/yy format 10/06/14 
		System.out.printf("Date in dd/mm/yy format %td/%tm/%ty %n", today,today,today ); 
		// The difference between %td and %te is that %td uses a 1eading zero e. p. 01�31 
		// whereas te does not e.g. 1�31 
		// tB means full, month name e.g. June 
		// te means day of month as 2�digits (no l.eading zero) 
		// tY means four�digit year 
		System.out.printf("Today is %tB %te, %tY %n", today,today,today);
		// Today is June 10, 2014
		
		
		 // Adding leading zeroes to numbers... 
		// %d is for decimal, 8 specifies the formatted number should be 8 charaters wide 
		// 0 means insert leading zeroes (if needed); the default is to pad with spaces, so if 
		// you don�t specify leading zeros, spaces will be used. 
		System.out.printf("Amount : 8d %n" , 221); // Amount : 221 
		System.out.printf("Amount : %08d %n" , 221);// Amount : 00000221 
		
		// printing positive and negative number using String format 
		// + means a�ways incluse the sign 
		System.out.printf("positive number : %+d %n", 1534632142); // positive number : +1534632142 
		System.out.printf("negative number : %+d %n", -989899); // negative number : �989899 
		
		System.out.printf("%f &n", Math.PI); // 3.141593 
		
		//3 digit after decimal point 
		System.out.printf("%.3f %n", Math.PI); // 3.142 
		
		//8 charcter in width and 3 digit after decimal point 
		System.out.printf("%8.3f %n", Math.PI); // � 3.142� 
		
		//adding commas into long numbers 
		// Total 10,000,000 messages processed today
		System.out.printf("Total %,d messages processed today", 10000000);
	}
}