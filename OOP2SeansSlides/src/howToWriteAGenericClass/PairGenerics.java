package howToWriteAGenericClass;

import java.io.ObjectInputStream.GetField;

//very nb how this is written
class Pair <T1, T2>{
	T1 object1;
	T2 object2;
	
	Pair(T1 one, T2 two){
		this.object1 = one;
		this.object2 = two;	
	}
	
	public T1 getFirstObj(){
		return this.object1;
	}
	
	public T2 getSecondObj(){
		return this.object2;
	}
}

public class PairGenerics {
	public static void main(String aa[]){
		//the order HAS to be the same, if compile error, its probably the order of
		//your types in the < > brackets
		//you can leave out the ones on the dynamic/right side to be safe if you want, 
		//but dont forget the empty <> still need to be there, and they come before the ().
		//if compile error its either the order of the types or the order of your brackets.
		Pair<Integer, String> worldCup = new Pair<Integer, String>(2010, "Africa");
		System.out.println("world cup for " + worldCup.getFirstObj() + " was in " + worldCup.getSecondObj());
		
	}
	

}
