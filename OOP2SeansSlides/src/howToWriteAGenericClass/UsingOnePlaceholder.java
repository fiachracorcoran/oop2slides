package howToWriteAGenericClass;

class PairOfT<T>{
	
	//note only using 1 <T> above for 2 objects (can be as many as you like) but T has to be the same type
	//see instantiation in public class below for clarify.
	T object1;
	T object2;
	
	public PairOfT(T one, T two) {
		this.object1 = one;
		this.object2 = two;
	}
	
	public T getFirstObj(){
		return this.object1;
	}
	
	public T getSecondObj(){
		return this.object2;
	}
	
}

public class UsingOnePlaceholder {

	public static void main(String gg []){
		//note the empty bracket are type inference
		PairOfT<String> names = new PairOfT<>("Emily", "Bobby");
		
	}
}
