package howToWriteAGenericClass;

//note the <T> in the class declaration, need that to tell compiler this is a generic class, else, compile error
class BoxPrinter<T> {
	private T val;
	//the type that T will be will be set down below in that other class when BoxPrinter is instantiated.
	public BoxPrinter(T argument){
		this.val = argument;
	}
	
	@Override
	public String toString(){
		//small reminder, without the "[" and "]" you'd have to cast val to a String here.
		return "[ " + val + "]";
	}
}
public class BoxPrinterTest{
	
	public static void main(String a[]){
		BoxPrinter<Integer> value1 = new BoxPrinter<Integer>(new Integer(10));
		System.out.println(value1);
		
		BoxPrinter<String> value2 = new BoxPrinter<String>("Hellso World");
		System.out.println(value2);
		
		//morale of the story, T is set to the type of each instantiation of it.
		
	}

}
