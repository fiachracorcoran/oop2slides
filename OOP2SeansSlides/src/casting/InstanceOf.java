package casting;

public class InstanceOf {
	public static void main(String[] args) {
		try{
			StringBuffer sb = new StringBuffer("Hello");
			Object obj = sb;
			String s = (String)obj;
		}catch(ClassCastException ex){
			// gnore as we dont want program to crash
		}
	}
	
	
	//Properly written
	public static void goodcode() {
		StringBuffer sb = new StringBuffer("Hello");
		Object obj = sb;
		if(obj instanceof String){//does obj refer to an object of type String
			System.out.println("String object");
			String s = (String)obj;
		}else if(obj instanceof StringBuffer){// does obj refer to an object of type StringBuffer
			System.out.println("StringBuffer object");
			StringBuffer sb2 = (StringBuffer)obj;
		}
	}

}
