package casting;

class Employee{
	
}
class Manager extends Employee{
	
}
class Director extends Manager{
	
}
class Engineer extends Employee{
	
}
public class Test {
	public static void main(String[] args) {
		//upcastting
		Director d = new Director();
		Manager m = new Manager();
		
		Employee e = m;	//ok - an employee ref can point to a Manager object
		m = d;			//ok - a Manager ref can point to a Director object
		e = d;			//ok - an Employee ref can point to a Director object
		
		m = (Manager)e;	// ok: would also work if e referred to a Director object
		Director dir = (Director)m; //ok at compile time fails at runtime
	
//		Engineer eng = (Engineer)m; //inconvertible types
	}
}
