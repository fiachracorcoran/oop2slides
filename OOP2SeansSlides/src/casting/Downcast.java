package casting;

class Downcast {
	public static void main(String []args) {
		Integer i = new Integer(10);
		
		//upcast - its fine and will always success
		Object obj = i;
		
		// downcast - fails at runtime
		String str = (String) obj;
	}
}
