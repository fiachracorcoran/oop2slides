package comparableInterface;
import java.util.*;

class StudentComparator implements Comparable<StudentComparable> {
    private String id, name;
    private Double cgpa;
    
    public StudentComparator(String studentId, String studentName, double studentCGPA) {
        this.id=studentId;
        this.name=studentName;
        this.cgpa=studentCGPA;
    }
    

    public String getName() {
        return name;
    }
    public String getId() {
        return id;
    }
    public Double getCgpa() {
        return cgpa;
    }
    
    
    @Override
    public String toString() {
        return id+" "+name+" "+cgpa;
    }


    
    //ignore this one in this class.
	@Override
	public int compareTo(StudentComparable that) {
		// TODO Auto-generated method stub
		return this.id.compareTo(that.getId());
	}
    
}
//different to comparable
    //N.B ======================================NB==================NB-============NB
class StudentCGPA2 implements Comparator<StudentComparable> {
    public int compare(StudentComparable s1, StudentComparable s2) {
        return s1.getCgpa().compareTo(s2.getCgpa());
    }
}

class MainClass {
    public static void main(String[] args) {
        StudentComparable[] students =    {   new StudentComparable("cs011", "Lennon", 3.1),
                                    new StudentComparable("cs021", "McCartney", 3.4),
                                    new StudentComparable("cs012", "Harrison", 2.7),
                                    new StudentComparable("cs022", "Starr", 3.7),
                                };
        //print them before sorting
        System.out.println(Arrays.toString(students));
        //sort theem
        //nbnbnbnbnbnbn  --- note the way it is done for comparator.
        Arrays.sort(students, new StudentCGPA2());
        
        //print them after sort.
        System.out.println(Arrays.toString(students));
    }
}