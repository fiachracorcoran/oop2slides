package comparableInterface;
import java.util.*;

class StudentComparable implements Comparable<StudentComparable> {
    private String id, name;
    private Double cgpa;
    
    public StudentComparable(String studentId, String studentName, double studentCGPA) {
        this.id=studentId;
        this.name=studentName;
        this.cgpa=studentCGPA;
    }
    

    public String getName() {
        return name;
    }
    public String getId() {
        return id;
    }
    public Double getCgpa() {
        return cgpa;
    }
    
    
    @Override
    public String toString() {
        return id+" "+name+" "+cgpa;
    }
    
    //N.B ======================================NB==================NB-============NB
    @Override
    public int compareTo(StudentComparable that) {
        return this.id.compareTo(that.id);
    }
}



class MainClass1 {
    public static void main(String[] args) {
        StudentComparable[] students =    {   new StudentComparable("cs011", "Lennon", 3.1),
                                    new StudentComparable("cs021", "McCartney", 3.4),
                                    new StudentComparable("cs012", "Harrison", 2.7),
                                    new StudentComparable("cs022", "Starr", 3.7),
                                };
        //print them before sorting
        System.out.println(Arrays.toString(students));
        //sort theem
        Arrays.sort(students);
        
        //print them after sort.
        System.out.println(Arrays.toString(students));
    }
}