package genericsErrors;

class BoxPrinter {
	private Object val;

	public BoxPrinter(Object arg) {
		val = arg;
	}

	public String toString() {
		return "[" + val + "]";
	}
	public Object getValue(){
		return val;
	}
}

public class BoxPrinterTest1 {
	public static void main(String[] args) {
		BoxPrinter value1 = new BoxPrinter(new Integer(10));
		System.out.println(value1);
		Integer intValue1 = (Integer) value1.getValue();
		
		BoxPrinter value2 = new BoxPrinter("Hello world");
		System.out.println(value2);
		
		//mistake here, did Integer cast, should do a String cast.
		//so "hello world" gets cast to an Integer, ClassCastException.
		Integer intValue2 = (Integer) value2.getValue();
	}
}