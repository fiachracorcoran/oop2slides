package genericsErrors;

import java.util.Vector;

//demos the lack of type safety in containers.
class OldContainerTest {
	public static void main(String []agrs){
		//vector stores objects of non-generic type
		Vector floatValues = new Vector();
		floatValues.add(10.0f);
		floatValues.add(100.0);
		
		for(int i=0;i<floatValues.size();i++){
			//float temp = (Float) floatValues.get(i);	//CLASS CAST EXCEPTION, double cannot be cast to a Float. (note the lack
														//of f after 100.0 above.
			System.out.println(floatValues.get(i));
		}

	}
}